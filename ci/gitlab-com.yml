---
image: python:3.9.18

stages:
  - lint
  - build
  - test
  - deploy-review
  - validate
  - release
  - runway

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  POETRY_CACHE_DIR: "$CI_PROJECT_DIR/.cache/poetry"

  DOCKER_VERSION: "20.10.23"
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_MODEL_GATEWAY: "$CI_REGISTRY_IMAGE/model-gateway"

  SAST_EXCLUDED_PATHS: "tests, tmp, api"

include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/SAST.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

  # Includes a base template for running kaniko easily
  # see https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/kaniko.md
  - project: "gitlab-com/gl-infra/common-ci-tasks"
    ref: v1.73.0 # renovate:managed
    file: "kaniko.yml"

  - project: "gitlab-com/gl-infra/platform/runway/ci-tasks"
    ref: v1.10.0 # renovate:managed
    file: "service-project/runway.yml"
    inputs:
      runway_service_id: ai-gateway
      image: "$CI_REGISTRY_IMAGE/model-gateway:${CI_COMMIT_SHORT_SHA}"
      runway_version: v1.10.0 # please keep in sync with `ref` above

cache:
  key:
    files:
      - poetry.lock
      - .gitlab-ci.yml
  paths:
    - $PIP_CACHE_DIR
    - $POETRY_CACHE_DIR
    - requirements.txt
    - scripts/lib/
    - scripts/vendor/

.poetry:
  before_script:
    - pip install poetry==1.5.1
    - poetry config virtualenvs.in-project true
    - poetry config cache-dir ${POETRY_CACHE_DIR}
    - poetry export -f requirements.txt --output requirements.txt --without-hashes
    - poetry config --list

.docker:
  image: docker:${DOCKER_VERSION}
  services:
    - docker:${DOCKER_VERSION}-dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

.docker-release-latest:
  extends: .docker
  script:
    - docker pull "${TARGET_IMAGE}"
    - |
      if [ -n "${RELEASED_IMAGE_COMMIT}" ]; then
        docker tag "${TARGET_IMAGE}" "${RELEASED_IMAGE_COMMIT}"
        docker push "${RELEASED_IMAGE_COMMIT}"
      fi
    - docker tag "${TARGET_IMAGE}" "${RELEASED_IMAGE_LATEST}"
    - docker push "${RELEASED_IMAGE_LATEST}"

lint:
  extends: .poetry
  stage: lint
  script:
    - make lint
    - poetry lock --no-update
    - git diff --exit-code

install:
  extends: .poetry
  stage: build
  script:
    - poetry install

build-docker-model-gateway:
  stage: build
  extends:
    - .kaniko_base
  variables:
    KANIKO_BUILD_FILE: Dockerfile
    KANIKO_DESTINATION: "${DOCKER_MODEL_GATEWAY}:${CI_COMMIT_SHORT_SHA}"

tests:
  extends: .poetry
  stage: test
  needs:
    - install
  script:
    - poetry install --with test
    - curl -sL https://deb.nodesource.com/setup_18.x | bash -
    - apt-get update && apt install -y nodejs build-essential git
    - poetry run python scripts/build-tree-sitter-lib.py
    - mv scripts/lib/tree-sitter-languages.so /usr/lib
    - poetry run pytest --junitxml=".test-reports/tests.xml"
  artifacts:
    when: always
    expire_in: 1 weeks
    reports:
      junit:
        - .test-reports/*.xml

gemnasium-python-dependency_scanning:
  stage: test
  needs:
    - install
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

secret_detection:
  stage: test
  needs:
    - install
  cache: {}
  rules:
    - if: $SECRET_DETECTION_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

semgrep-sast:
  stage: test
  needs:
    - install
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

release-docker-model-gateway:
  stage: release
  extends: .docker-release-latest
  variables:
    TARGET_IMAGE: "$DOCKER_MODEL_GATEWAY:$CI_COMMIT_SHORT_SHA"
    RELEASED_IMAGE_LATEST: "$DOCKER_MODEL_GATEWAY:latest"
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
