# Fauxpilot is deployed via a Helm chart

See the [`ai-assist` chart](../../infrastructure/ai-assist/) for details of deploying `fauxpilot`.